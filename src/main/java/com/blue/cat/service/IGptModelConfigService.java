package com.blue.cat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blue.cat.bean.entity.GptModelConfig;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lixin
 * @since 2023-08-01
 */
public interface IGptModelConfigService extends IService<GptModelConfig> {

}
