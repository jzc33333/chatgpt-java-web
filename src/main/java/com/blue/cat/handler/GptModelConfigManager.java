package com.blue.cat.handler;

import com.blue.cat.bean.entity.GptModelConfig;
import com.blue.cat.bean.vo.GptModelConfigVo;
import com.blue.cat.service.impl.GptModelConfigServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GptModelConfigManager {

    @Autowired
    private GptModelConfigServiceImpl gptModelConfigService;

    /**
     * 获取所有有效的token
     * @return
     */
    public List<GptModelConfigVo> getAllValidGptConfig(){
        List<GptModelConfig> gptModelConfigs = gptModelConfigService.getBaseMapper().getAllValidGptConfig();
        return gptModelConfigs.stream().map(e -> {
            return GptModelConfigVo.builder()
                    .baseUrl(e.getBaseUrl())
                    .model(e.getModel())
                    .token(e.getToken())
                    .weight(e.getWeight()).build();
        }).collect(Collectors.toList());
    }

}
